﻿/*
 * Created by SharpDevelop.
 * User: zach.read
 * Date: 15-07-16
 * Time: 16:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace EL_Image_Locator
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{		
		public MainForm()
		{	
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			textBox2.Text = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\ELImages";
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		void Button1Click(object sender, EventArgs e)
		{
			DialogResult result = folderBrowserDialog1.ShowDialog();
		    if (result == DialogResult.OK)
		    {
				//
				// The user selected a folder and pressed the OK button.
				// We print the number of files found.
				//
				textBox2.Text = folderBrowserDialog1.SelectedPath.ToString();
		    }
		}
		void btnGetClick(object sender, EventArgs e)
		{					
			string[] lines = null;
				
			if(textBox1.Text.Length < 13){
				MessageBox.Show("Please enter a valid serial number.");
				return;
			}
			
			if(String.IsNullOrEmpty(sourceCB.Text)){
				MessageBox.Show("Please select what EL image scource you want to search.");
				return;
			}
			lines = textBox1.Text.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
			
			foreach (string s in lines){
				if(sourceCB.Text == "EL2"){
					SearchImages(s, s.Substring(1,13), @"\\CA01A9004\EL_Tester\Images\", @"\\ca01a9004\EL_Tester\UnsortedImages\Accepted\", @"\\ca01a9004\EL_Tester\UnsortedImages\Rejected\");
				}
				else{
					SearchImages(s, s, @"\\ca01a9004\EL_Tester\EOL_EL_Tester\Stored\Images\", @"\\ca01a9004\EL_Tester\EOL_EL_Tester\Accepted\", @"\\ca01a9004\EL_Tester\EOL_EL_Tester\Rejected\");
				}
			}
		}
		
		void SearchImages(string sn, string snLive, string StoredPath, string LivePathAcc, string LivePathRej){			
			string findpath = "";
			string message = "";
			string logfile = "";
			int notfound = 0;
			string[] filefound = null;
			string[] dirs;	
				
			string curpath = Directory.GetCurrentDirectory();
			System.IO.Directory.CreateDirectory(textBox2.Text);
			
			if (!Directory.Exists(textBox2.Text))
			{
				MessageBox.Show("Please select a vaild path to save the images.");
				return;
			}
			
			notfound = 0;
			if(sn.Length == 13)
				findpath = StoredPath + sn.Substring(2,2).ToString() + @"\" + sn.Substring(1,1).ToString() + @"\" + sn.Substring(6,3).ToString() + @"\";
			if(sn.Length == 14)
				findpath = StoredPath + sn.Substring(1,2).ToString() + @"\" + sn.Substring(5,2).ToString() + @"\" + sn.Substring(7,3).ToString() + @"\";	
							
			if(Directory.Exists(findpath)){
				filefound = Directory.GetFiles(findpath, sn + "*", SearchOption.AllDirectories);								
				
				if(filefound != null && filefound.Length != 0){
					foreach (string f in filefound) {						
						System.IO.File.Copy(f, textBox2.Text + @"\" + Path.GetFileName(f), true);
						logfile = logfile + Path.GetFileName(f) + " was found." + Environment.NewLine;
					}
				}
				else{
					notfound = 1;						
				}
			}
			else{
				notfound = 1;					
			}
			findpath = "";
			
			if(notfound == 1 && sn != "" && (sn.Length == 13 || sn.Length == 14)){
				if(sn.Length == 13)
					dirs = Directory.GetFiles(LivePathAcc, snLive + "*", SearchOption.AllDirectories);
				else
					dirs = Directory.GetFiles(LivePathAcc, snLive + "*", SearchOption.AllDirectories); // can't be substring for EL3 durrr
				if(dirs != null && dirs.Length != 0){
					foreach (string t in dirs) {
						System.IO.File.Copy(t, textBox2.Text + @"\" + Path.GetFileName(t), true);
						logfile = logfile + Path.GetFileName(t) + " was found." + Environment.NewLine;
					}
				}
				else{
					if(sn.Length == 13)
						dirs = Directory.GetFiles(LivePathRej, snLive + "*", SearchOption.AllDirectories);						
					else
						dirs = Directory.GetFiles(LivePathRej, snLive + "*", SearchOption.AllDirectories);
					if(dirs != null && dirs.Length != 0){
						foreach (string t in dirs) {
							System.IO.File.Copy(t, textBox2.Text + @"\" + Path.GetFileName(t), true);
							logfile = logfile + Path.GetFileName(t) + " was found." + Environment.NewLine;
						}
					}
					else if(notfound == 1){
						message = message + ", " + sn;
						logfile = logfile + sn + " was NOT found." + Environment.NewLine;
					}
				}					
			}
					
			if(message.Length > 0)
				MessageBox.Show("Images not found: " + message);
			else
				MessageBox.Show("All images found");
			textBox1.Clear();
			
			System.IO.File.WriteAllText(curpath + @"\logfile.txt", logfile);
			
			if(Directory.Exists(textBox2.Text))			
			    Process.Start("explorer.exe", textBox2.Text);
		}
		
		void Button3Click(object sender, EventArgs e)
		{
			if(Directory.Exists(textBox2.Text))			
			    Process.Start("explorer.exe", textBox2.Text);
			else
				MessageBox.Show("Please enter a vaild save location.");
		}
	}
}

﻿/*
 * Created by SharpDevelop.
 * User: zach.read
 * Date: 15-07-16
 * Time: 16:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace EL_Image_Locator
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnGet;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button btnLocation;
		private System.Windows.Forms.Label Version;
		private System.Windows.Forms.Label versionLbl;
		private System.Windows.Forms.ComboBox sourceCB;
		private System.Windows.Forms.Label label3;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnGet = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.btnLocation = new System.Windows.Forms.Button();
			this.Version = new System.Windows.Forms.Label();
			this.versionLbl = new System.Windows.Forms.Label();
			this.sourceCB = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBox1.Location = new System.Drawing.Point(438, 84);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox1.Size = new System.Drawing.Size(211, 282);
			this.textBox1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(438, 58);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(180, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "Serial Numbers:";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(43, 58);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(180, 23);
			this.label2.TabIndex = 2;
			this.label2.Text = "Save Location:";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(46, 96);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(314, 20);
			this.textBox2.TabIndex = 3;
			// 
			// btnSave
			// 
			this.btnSave.Font = new System.Drawing.Font("Monospace 821", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSave.Location = new System.Drawing.Point(359, 96);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(40, 20);
			this.btnSave.TabIndex = 4;
			this.btnSave.Text = "...";
			this.btnSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.Button1Click);
			// 
			// btnGet
			// 
			this.btnGet.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnGet.Location = new System.Drawing.Point(141, 213);
			this.btnGet.Name = "btnGet";
			this.btnGet.Size = new System.Drawing.Size(184, 52);
			this.btnGet.TabIndex = 5;
			this.btnGet.Text = "Get Images";
			this.btnGet.UseVisualStyleBackColor = true;
			this.btnGet.Click += new System.EventHandler(this.btnGetClick);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(1, 1);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(205, 37);
			this.pictureBox1.TabIndex = 6;
			this.pictureBox1.TabStop = false;
			// 
			// btnLocation
			// 
			this.btnLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnLocation.Location = new System.Drawing.Point(111, 303);
			this.btnLocation.Name = "btnLocation";
			this.btnLocation.Size = new System.Drawing.Size(249, 52);
			this.btnLocation.TabIndex = 7;
			this.btnLocation.Text = "Open Image Location";
			this.btnLocation.UseVisualStyleBackColor = true;
			this.btnLocation.Click += new System.EventHandler(this.Button3Click);
			// 
			// Version
			// 
			this.Version.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Version.Location = new System.Drawing.Point(594, 399);
			this.Version.Name = "Version";
			this.Version.Size = new System.Drawing.Size(74, 23);
			this.Version.TabIndex = 8;
			this.Version.Text = "Version:";
			// 
			// versionLbl
			// 
			this.versionLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.versionLbl.Location = new System.Drawing.Point(658, 399);
			this.versionLbl.Name = "versionLbl";
			this.versionLbl.Size = new System.Drawing.Size(34, 23);
			this.versionLbl.TabIndex = 9;
			this.versionLbl.Text = "4.0.0.0";
			// 
			// sourceCB
			// 
			this.sourceCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.sourceCB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.sourceCB.FormattingEnabled = true;
			this.sourceCB.Items.AddRange(new object[] {
			"EL2",
			"EL3"});
			this.sourceCB.Location = new System.Drawing.Point(233, 146);
			this.sourceCB.Name = "sourceCB";
			this.sourceCB.Size = new System.Drawing.Size(92, 32);
			this.sourceCB.TabIndex = 10;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(141, 150);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(86, 24);
			this.label3.TabIndex = 11;
			this.label3.Text = "Source:";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(692, 422);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.sourceCB);
			this.Controls.Add(this.versionLbl);
			this.Controls.Add(this.Version);
			this.Controls.Add(this.btnLocation);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.btnGet);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBox1);
			this.Name = "MainForm";
			this.Text = "EL Image Locator";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
